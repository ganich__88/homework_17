#include <iostream>
#include <cmath>


class Vector
{
private:
    double x;
    double y;
    double z;
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    double GetVector()
    {
        return sqrt(x * x + y * y + z * z);
    }
    void SetVector(double newX, double newY, double newZ)
    {
        x = newX;
        y = newY;
        z = newZ;
    }
    void Show()
    {
        std::cout << '\n' << x << " " << y << " " << z << std::endl;
        std::cout << sqrt(x * x + y * y + z * z) << std::endl;
    }
    
};

int main()
{
    Vector v(20, 35, 17);
    v.Show();
    v.SetVector(20, 35, 17);
    std::cout << '\n' << v.GetVector() << std::endl;
    
    return 0;
}